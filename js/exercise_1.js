/* 
(*) SORT 3 INTEGERS IN ASCENDING ORDER

Input
- Users input 3 integers accordingly to sort and is stored in variables "num1" to "num3" respectively

To-do
- If num1 is greater than num2, swap the values of num1 and num2, if not do nothing.
- If num2 is greater than num3, swap the values of num2 and num3, if not do nothing.
- After completing two step above, num3 will store the biggest number. After that we need to determine is which variable between num1 and num2 has a greater value.
- If num1 is greater than num2, swap the values of num1 and num2.
- After that the three integers is sorted.

Output
- The three integers with ascending order are shown to the users
*/

function sort() {
  let num1 = document.querySelector("#sort__num-1").value * 1;
  let num2 = document.querySelector("#sort__num-2").value * 1;
  let num3 = document.querySelector("#sort__num-3").value * 1;

  if (num1 > num2) {
    [num1, num2] = [num2, num1];
  }
  if (num2 > num3) {
    [num2, num3] = [num3, num2];
  }
  if (num1 > num2) {
    [num1, num2] = [num2, num1];
  }

  document.querySelector(
    "#result_sort"
  ).textContent = `${num1} < ${num2} < ${num3}`;
}
