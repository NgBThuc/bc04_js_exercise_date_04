/* 
(*) COUNT EVEN AND ODD NUMBER

Input
- The variables from "num1" to "num3" store the 3 integers entered by the users accordingly.

To-do
- The number of odd numbers and even numbers are stored in the variables "countOdd" and "countEven" respectively.
- If num1 is divisible by 2, increase the value of the variable "countEven" by 1. if not increase the value of the variable "countOdd" by 1.
- If num2 is divisible by 2, increase the value of the variable "countEven" by 1. if not, increase the value of the variable "countOdd" by 1.
- If num3 is divisible by 2, increase the value of the variable "countEven" by 1. if not, increase the value of the variable "countOdd" by 1.

Output
- The value of the variables "countOdd" and "countEven" are shown to the users.
*/

function checkEvenOdd() {
  let num1 = document.querySelector("#evenOdd__num-1").value * 1;
  let num2 = document.querySelector("#evenOdd__num-2").value * 1;
  let num3 = document.querySelector("#evenOdd__num-3").value * 1;
  let countOdd = 0;
  let countEven = 0;

  if (num1 % 2 === 0) {
    countEven++;
  } else {
    countOdd++;
  }

  if (num2 % 2 === 0) {
    countEven++;
  } else {
    countOdd++;
  }

  if (num3 % 2 === 0) {
    countEven++;
  } else {
    countOdd++;
  }

  document.querySelector(
    "#result_evenOdd"
  ).textContent = `Có ${countEven} số chẵn và ${countOdd} số lẻ`;
}
