/* 
(*) GUESS TRIANGLE

Input
- Users enter the values of the three sides of the triangle.
- The three sides of the triangle are stored in the variables "side1" to "side3" respectively.

To-do
- If the three sides of a triangle are equal to each other, then it is an equilateral triangle.
- If not, consider the Pythagorean theorem to determine the right-angled triangle.
- If not, consider the condition that if any two sides of a triangle are equal to each other, then it is defined as an isosceles triangle.
- If not, it is defined to be another type of triangle.
- The type of the triangle is stored in variable "result"

Output
- The "result" is shown to the users

*/

function guessTriangle() {
  let side1 = document.querySelector("#triangle__num-1").value * 1;
  let side2 = document.querySelector("#triangle__num-2").value * 1;
  let side3 = document.querySelector("#triangle__num-3").value * 1;
  let result = "";

  if (side1 === side2 && side2 === side3) {
    result = "Tam giác đều";
  } else if (
    side1 ** 2 === side2 ** 2 + side3 ** 2 ||
    side2 ** 2 === side1 ** 2 + side3 ** 2 ||
    side3 ** 2 === side1 ** 2 + side2 ** 2
  ) {
    result = "Tam giác vuông";
  } else if (side1 === side2 || side2 === side3 || side1 === side3) {
    result = "Tam giác cân";
  } else {
    result = "Tam giác khác";
  }

  document.querySelector("#result_triangle").textContent = result;
}
