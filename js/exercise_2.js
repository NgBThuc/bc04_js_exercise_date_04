/* 
(*) GREETING

Input
- The users will select the person to be greeted by selecting the value on the dropdown menu.

To-do
- Take the corresponding value from the selected item.
- If the value is equal to "B", varible "result" will equal to "Xin chào Bố"
- If the value is equal to "M", varible "result" will equal to "Xin chào Mẹ"
- If the value is equal to "A", varible "result" will equal to "Xin chào Anh Trai"
- If the value is equal to "E", varible "result" will equal to "Xin chào Em Gái"

Output
- The variable "result" will be shown to the users
*/

function greeting() {
  let greetingTarget = document.querySelector("#greeting__dropdown").value;
  let result = "";

  switch (greetingTarget) {
    case "B":
      result = "Xin chào Bố";
      break;
    case "M":
      result = "Xin chào Mẹ";
      break;
    case "A":
      result = "Xin chào Anh Trai";
      break;
    case "E":
      result = "Xin chào Em Gái";
      break;
  }

  document.querySelector("#result_greeting").textContent = result;
}
